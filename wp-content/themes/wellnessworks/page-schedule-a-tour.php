<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */

get_header();
//get_template_part('header', 'promo');
/*
 * Template Name: Schedule A Tour
 */
?>

    <div id="banner" class="page">
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
        <!-- Start of Meetings Embed Script -->
  		  <div class="meetings-iframe-container" data-src="https://app.hubspot.com/meetings/join1/book-a-tour?		embed=true"></div>
    		<script type="text/javascript" src="https://static.hsappstatic.net/MeetingsEmbed/ex/MeetingsEmbedCode.js"></script>
 		<!-- End of Meetings Embed Script -->
				<h2 class="">
					<small>Need another time?</small></h2>
					<br>
					Contact us at <a href="mailto:join@wellnessworks.nyc">join@wellnessworks.nyc</a> or use the form below to schedule a weekend tour or visit outside of normal business hours.
            </div>
            <div class="col-md-8 col-md-offset-2">
                <hr>
                <h2 class="">
                    <small>Do you have any questions?</small>
                    <br>
                    Contact WellnessWorks
                </h2>
                <br><br>
                <!--[if lte IE 8]>
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                <![endif]-->
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                    hbspt.forms.create({
                        css: '',
                        portalId: '3922905',
                        formId: 'ded28c62-e843-4cd8-aa58-2e8c8e440a33'
                    });
                </script>
            </div>
        </div>
    </div>

<?php
get_sidebar();
get_footer();
//get_template_part('footer', 'promo');
