<?php
        /**
         * The header for our theme
         *
         * This is the template that displays all of the <head> section and everything up until <div id="content">
         *
         * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
         *
         * @package Wellness_Works
         */

        ?><!DOCTYPE html>
        <html <?php language_attributes(); ?>>
        <head>
            <meta charset="<?php bloginfo('charset'); ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="portal" content="<?php echo PORTAL; ?>">
            <meta name="token" content="<?php echo wp_create_nonce(); ?>">
            <link rel="profile" href="https://gmpg.org/xfn/11">
            <link rel="apple-touch-icon" sizes="180x180" href="https://wellnessworks.nyc/wp-content/themes/wellnessworks/images/favicons/apple-touch-icon.png">
            <link rel="icon" type="image/png" sizes="32x32" href="https://wellnessworks.nyc/wp-content/themes/wellnessworks/images/favicons/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="16x16" href="https://wellnessworks.nyc/wp-content/themes/wellnessworks/images/favicons/favicon-16x16.png">
            <link rel="manifest" href="https://wellnessworks.nyc/wp-content/themes/wellnessworks/images/favicons/manifest.json">
            <link rel="mask-icon" href="https://wellnessworks.nyc/wp-content/themes/wellnessworks/images/favicons/safari-pinned-tab.svg" color="#000000">
            <link rel="shortcut icon" href="https://wellnessworks.nyc/wp-content/themes/wellnessworks/images/favicons/favicon.ico">
            <meta name="msapplication-config" content="https://wellnessworks.nyc/wp-content/themes/wellnessworks/images/favicons/browserconfig.xml">
            <meta name="theme-color" content="#ffffff">
            <?php wp_head(); ?>
            <script src="https://use.typekit.net/gfk5jah.js"></script>
            <script>try{Typekit.load({ async: true });}catch(e){}</script>
            <!-- Facebook Pixel Code -->
            <script>
                !function(f,b,e,v,n,t,s)
                {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                    n.queue=[];t=b.createElement(e);t.async=!0;
                    t.src=v;s=b.getElementsByTagName(e)[0];
                    s.parentNode.insertBefore(t,s)}(window,document,'script',
                    'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '225842217941516');
                fbq('track', 'PageView');
            </script>
            <noscript>
                <img height="1" width="1"
                     src="https://www.facebook.com/tr?id=225842217941516&ev=PageView
&noscript=1"/>
            </noscript>
            <!-- End Facebook Pixel Code -->
        </head>

        <body <?php body_class(); ?>>
        <script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5798305"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5798305&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-105158698-1', 'auto');
            ga('send', 'pageview');

        </script>
        <div id="page" class="site">

            <header id="masthead" class="site-header" role="banner">
                <div class="container">
                    <div class="site-branding col-md-4">
                        <a href="<?php echo esc_url(home_url('/')); ?>" class="brand">
                            <img src="<?php echo get_template_directory_uri().'/images/logo-white.png'?>" class="hidden-sm hidden-xs" style="width:100%;max-width:350px;" alt="Wellness Works - Full Service Suites For The Practitioner Community">
                            <img src="<?php echo get_template_directory_uri().'/images/logo.png'?>"  class="hidden-lg hidden-md" style="width:100%;max-width:350px;" alt="Wellness Works - Full Service Suites For The Practitioner Community">
                        </a>
                    </div>
                    <!-- .site-branding -->

                    <nav id="site-navigation" class="main-navigation col-md-8 text-center" role="navigation" style="position:relative;z-index:9999">
                        <div class="text-center hidden-lg hidden-md">
                            <br>
                            <a data-toggle="collapse" data-target="#nav-collapse" style="font-size:18px;position: relative;z-index:9999;" class="btn btn-xs"><i class="fa fa-bars" aria-hidden="true"></i></a>
                        </div>
                        <div class="collapse navbar-collapse" id="nav-collapse">
                            <?php wp_nav_menu(array('theme_location' => 'menu-1', 'menu_id' => 'primary-menu', 'items_wrap' => with_login())); ?>
                        </div>
                    </nav>
                    <!-- #site-navigation -->
                </div>
            </header>
            <!-- #masthead -->

            <div id="content" class="site-content">
