<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */

get_header(); ?>

    <div id="banner" class="page">
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">



            <?php
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>

            <?php get_template_part('template-parts/content', 'four-core'); ?>

            <div class="container">
                <div class="clearfix"></div>
                <div class="row">
                    <?php get_template_part( 'template-parts/content', 'services' );?>
                </div>
                <div class="clearfix"></div>

                <div class="products">
                    <?php

                    $args = [
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'posts_per_page' => -1,
                        'order' => 'ASC',
                        'orderby' => [
                            'meta_key' => 'sort_order'
                        ]
                    ];
                    $the_query = new WP_Query($args); ?>

                    <?php if ($the_query->have_posts()) : ?>


                        <!-- the loop -->
                        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                            <div class="<?php the_field('width'); ?>">
                                <div class="product-list <?php the_field('tile_style'); ?> product-services">
                                    <div class="product-title">
                                        <h2 style="text-transform:uppercase"><?php the_title(); ?></h2>
                                    </div>
                                    <div class="product-content">
                                        <?php if(get_field('type') == 'enterprise'):?>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-12">
                                                    <ul>
                                                        <?php

                                                        // check if the repeater field has rows of data
                                                        if( have_rows('first_column_service_list') ):

                                                            // loop through the rows of data
                                                            while ( have_rows('first_column_service_list') ) : the_row();

                                                                // display a sub field value
                                                                echo '<li>'.get_sub_field('name').'</li>';

                                                            endwhile;

                                                        else :

                                                            // no rows found

                                                        endif;

                                                        ?>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <ul>
                                                        <?php

                                                        // check if the repeater field has rows of data
                                                        if( have_rows('second_column_service_list') ):

                                                            // loop through the rows of data
                                                            while ( have_rows('second_column_service_list') ) : the_row();

                                                                // display a sub field value
                                                                echo '<li>'.get_sub_field('name').'</li>';

                                                            endwhile;

                                                        else :

                                                            // no rows found

                                                        endif;

                                                        ?>
                                                    </ul>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="col-md-12">
                                                        <h2><?php the_field('callout'); ?></h2>
                                                        <a href="/schedule-a-tour" class="btn btn-primary">Schedule A Tour</a>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 text-center">
                                                    <h3 style="padding-top:0;margin-top:15px;"><?php the_field('slogan'); ?></h3>
                                                </div>
                                            </div>
                                        <?php else:?>
                                            <ul>
                                                <?php

                                                // check if the repeater field has rows of data
                                                if( have_rows('service_list') ):

                                                    // loop through the rows of data
                                                    while ( have_rows('service_list') ) : the_row();

                                                        // display a sub field value
                                                        echo '<li>'.get_sub_field('name').'</li>';

                                                    endwhile;

                                                else :

                                                    // no rows found

                                                endif;

                                                ?>
                                            </ul>
                                        <?php endif;?>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php if(get_field('type') == 'standard'):?>
                                        <div class="product-service-actions " style="margin-top:-45px;">

                                            <div class="product-actions">
                                                <a href="/membership" class="col-md-6 col-sm-12 col-xs-12 product-action action-brown">Learn More</a>
                                                <a href="/schedule-a-tour" class="col-md-6 col-sm-12 col-xs-12 product-action action-blue">Schedule a Tour</a>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    <?php endif;?>
                                </div>
                            </div>
                        <?php endwhile; ?>
                        <!-- end of the loop -->

                        <?php wp_reset_postdata(); ?>
                    <?php endif; ?>

                </div>
                <hr>
                <div class="clearfix"></div>
                <br><br>
                <div class="clearfix"></div>
                <div class="well">
                    <p>
                        <strong>*</strong>= For additional Fee
                        <strong>**</strong>= Park Slope location opening in late 2017
                    </p>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
