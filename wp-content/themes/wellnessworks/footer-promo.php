<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Wellness_Works
 */

?>

</div><!-- #content -->
<footer class="site-footer" style="min-height:75px;padding-top:20px;padding-bottom:20px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="lead">
                    <a href="https://www.facebook.com/wellnessworkshq/" target="_blank" style="color:#fff;"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="col-md-12 text-center">
<!--                <img src="--><?php //echo get_template_directory_uri().'/images/logo-white.png'?><!--" style="width:100%;max-width:320px;" alt="">-->
<!--                <br><br>-->
                <p style="color:#fff;">Full Site coming soon. &copy; 2017 WellnessWorks at Trinity Centre LLC. 115 Broadway, New York, NY</p>
            </div>
<!--            <div class="col-md-6 text-right">-->
<!--                <a href="mailto:join@wellnessworks.nyc" style="color:#fff;" class="lead">join@wellnessworks.nyc</a>-->
<!--            </div>-->
        </div>
    </div>
</footer>
</div><!-- #page -->

<script
    src="https://code.jquery.com/jquery-3.2.1.min.js"
    integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
    crossorigin="anonymous"></script>
<?php wp_footer(); ?>

</body>
</html>
