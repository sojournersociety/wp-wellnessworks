<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */

get_header(); ?>

    <div id="banner" class="page">
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>

            <div class="container">
                <?php

                $args = [
                    'post_type' => 'suite',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'meta_key' => 'sort_order',
                    'orderby' => 'meta_value_num',
                    'order' => 'ASC'
                ];
                $the_query = new WP_Query($args); ?>

                <?php if ($the_query->have_posts()) : ?>

                    <!-- pagination here -->
                    <?php $counter = 0;?>
                    <!-- the loop -->
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="col-md-4 suite-listing">
                            <div class="panel panel-default" id="<?php echo strtolower(str_replace(' ', '-', get_the_title())); ?>">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-12 thumb">
                                            <img src="<?php the_field('image_1'); ?>" alt=""/>
                                        </div>
                                        <div class="col-md-12">
                                            <h4><?php the_title(); ?></h4>
                                            <br>
                                        </div>
                                        <div class="col-md-12 ideal-for">
                                            <p>
                                                <small class="text-muted"><?php echo get_field('for_tour_only') ? 'For Those Looking For' : 'Ideal For'; ?></small>
                                                <br>
                                                <?php the_field('ideal_for'); ?>
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-12 suite-description">
                                            <?php the_content(); ?>
                                        </div>

                                        <div class="credits-container col-md-12 <?php echo get_field('for_tour_only') ? 'hidden' : ''; ?>">
                                            <div class="well credits">
                                                <p>
                                                    <?php if(strlen(get_field('credits')) > 0 || strlen(get_field('price')) > 0):?>
                                                    <strong><small>Starting at</small></strong>
                                                        <?php echo strlen(get_field('credits')) > 0 ? '<br/><strong>'.get_field('credits').' Credits Per Hour'.'</strong><br/>' : ''; ?>
                                                    <<?php echo strlen(get_field('price')) > 0 ? 'strong>$'.get_field('price').' Per Hour</strong>' : ''; ?>
                                                    <?php endif;?>
                                                </p>
<!--                                                <small>-->
<!--                                                    WellnessWorks Flex & Flex PLus Members can use their credits to reserve this suite.-->
<!--                                                </small>-->
<!--                                                <br><br>-->
<!--                                                <a href="/membership" class="btn btn-primary">Memberships</a>-->
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <br>
                                        <a href="/schedule-a-tour" class="btn btn-secondary btn suite-actions suite-tour col-xs-10 col-xs-offset-1">Schedule a Tour</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php $counter++;
                            echo $counter % 3 == 0 ? '<div class="clearfix"></div>' : '';
                        ?>
                    <?php endwhile; ?>
                    <!-- end of the loop -->

                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
            <?php get_template_part( 'template-parts/content', 'services' );?>

        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
