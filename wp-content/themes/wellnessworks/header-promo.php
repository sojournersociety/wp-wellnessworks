<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Wellness_Works
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="portal" content="<?php echo PORTAL; ?>">
    <meta name="token" content="<?php echo wp_create_nonce(); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <?php wp_head(); ?>
    <script src="https://use.typekit.net/gfk5jah.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window,document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '225842217941516');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1"
             src="https://www.facebook.com/tr?id=225842217941516&ev=PageView
&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>

<body <?php body_class(); ?>>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-105158698-1', 'auto');
    ga('send', 'pageview');

</script>
<div id="page" class="site">

    <header id="masthead" class="site-header" role="banner">
        <div class="container">
            <div class="site-branding col-md-4">
                <a href="<?php echo esc_url(home_url('/')); ?>" class="brand">
                    <img src="<?php echo get_template_directory_uri().'/images/logo-white.png'?>" class="hidden-sm hidden-xs" style="width:100%;max-width:350px;" alt="Wellness Works - Full Service Suites For The Practitioner Community">
                    <img src="<?php echo get_template_directory_uri().'/images/logo.png'?>"  class="hidden-lg hidden-md" style="width:100%;max-width:350px;" alt="Wellness Works - Full Service Suites For The Practitioner Community">
                </a>
            </div>
            <!-- .site-branding -->

<!--            <nav id="site-navigation" class="main-navigation col-md-8 text-center" role="navigation">-->
<!--                --><?php //wp_nav_menu(array('theme_location' => 'promo-1', 'menu_id' => 'promo-header')); ?>
<!--            </nav>-->
            <!-- #site-navigation -->
        </div>
    </header>
    <!-- #masthead -->

    <div id="content" class="site-content">
