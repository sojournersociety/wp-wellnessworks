<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */

get_header(); ?>
    <div id="banner" class="page">
    </div>
    <div id="banner" class="home" style="background-image:url(
    <?php
    while ( have_posts() ) : the_post();
        if(has_post_thumbnail()) {
            echo the_post_thumbnail_url('full');
        } else {
            echo get_template_directory_uri().'/images/banner.png';
        }
    endwhile; // End of the loop.
    ?>
        );background-position: top center !important;">
    </div>
    <div class="container">
        <div class="col-md-8 col-md-offset-2 text-center">
            <h1><?php the_field('headline'); ?> <br> <a href="<?php the_field('main_button_link'); ?>" class="btn btn-primary btn-lg"><?php the_field('main_button_text'); ?></a></h1>
            <br>
            <h4><?php the_field('tagline'); ?></h4>
        </div>

        <hr class="divider col-md-8 col-md-offset-2">

        <div class="col-md-12">
            <h1 class="text-center"><?php the_field('difference_heading'); ?> </h1>
            <br><br>
        </div>
    </div>

<?php get_template_part('template-parts/content', 'four-core'); ?>

<?php
$query = new WP_Query(array('post_type' => 'pull_quote', 'post' => 42));
if ($query->have_posts()) : ?>
    <?php while ($query->have_posts()) : $query->the_post(); ?>
        <div class="pull-quote">
            <h1 class="text-center">
                <?php the_content(); ?>
                <a href="/schedule-a-tour" class="btn btn-default btn-lg btn-yellow">Schedule A Tour</a>
            </h1>
        </div>
    <?php endwhile;
    wp_reset_postdata(); ?>
<?php endif; ?>


    <div class="container">
        <h1 class="page-header text-center">Pricing Options</h1>
        <div class="products">
            <?php

            $args = [
                'post_type' => 'product',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'order' => 'ASC',
                'orderby' => [
                    'meta_key' => 'sort_order'
                ]
            ];
            $the_query = new WP_Query($args); ?>

            <?php if ($the_query->have_posts()) : ?>


                <!-- the loop -->
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <div class="<?php the_field('width'); ?>">
                        <div class="product-list <?php the_field('tile_style'); ?>">
                            <div class="product-title">
                                <h2 style="text-transform:uppercase"><?php the_title(); ?></h2>
                            </div>
                            <div class="product-content">
                                <?php if(get_field('type') == 'enterprise'):?>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <ul>
                                                <?php

                                                // check if the repeater field has rows of data
                                                if( have_rows('first_column_product_list') ):

                                                    // loop through the rows of data
                                                    while ( have_rows('first_column_product_list') ) : the_row();

                                                        // display a sub field value
                                                        echo '<li>'.get_sub_field('name').'</li>';

                                                    endwhile;

                                                else :

                                                    // no rows found

                                                endif;

                                                ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <ul>
                                                <?php

                                                // check if the repeater field has rows of data
                                                if( have_rows('second_column_product_list') ):

                                                    // loop through the rows of data
                                                    while ( have_rows('second_column_product_list') ) : the_row();

                                                        // display a sub field value
                                                        echo '<li>'.get_sub_field('name').'</li>';

                                                    endwhile;

                                                else :

                                                    // no rows found

                                                endif;

                                                ?>
                                            </ul>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <h2 style="line-height:1.5;"><?php the_field('slogan'); ?></h2>
                                        </div>
                                    </div>
                                <?php else:?>
                                    <ul>
                                        <li>
                                            <?php the_field('base_information'); ?>
                                        </li>
                                        <li>
                                            <?php the_content(); ?>
                                        </li>
                                    </ul>
                                <?php endif;?>
                            </div>
                            <?php if(get_field('type') == 'standard'):?>
                                <div class="product-price">
                                    <h1><?php the_field('price'); ?></h1>
                                </div>
                            <?php endif;?>
                            <div class="clearfix"></div>
                            <div class="product-actions">
                                <a href="/services" class="col-md-6 col-sm-12 col-xs-12 product-action action-brown">View Services</a>
                                <a href="/schedule-a-tour" class="col-md-6 col-sm-12 col-xs-12 product-action action-blue">Schedule a Tour</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                <?php endwhile; ?>
                <!-- end of the loop -->

                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div>


    <div class="clearfix"></div>
<?php
//get_sidebar();
get_footer();
