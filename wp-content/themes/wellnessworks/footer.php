<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Wellness_Works
 */

?>

	</div><!-- #content -->
	<footer class="site-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4 <?php echo strlen(get_field('footer_tagline_heading')) == 0 ? 'col-md-offset-2' : ''; ?>">
					<h2 style="margin-bottom:-10px;padding-bottom:0;">Our Offices</h2>
					<ul class="location-list api-locations">
						<img src="<?php echo get_template_directory_uri().'/images/loader.gif'; ?>" style="width:100px;display:block;" alt="">
					</ul>
				</div>
				<?php if(strlen(get_field('footer_tagline_heading')) > 0):?>
				<div class="col-md-4">
					<?php

					$args = [
						'post_type' => 'page',
//						'post_status' => 'publish',
						'p' => 7,
						'posts_per_page' => 1
					];
					$the_query = new WP_Query($args); ?>

					<?php if ($the_query->have_posts()) : ?>


						<!-- the loop -->
						<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
							<div class="tagline">
								<h4><span class="lead text-center" style="text-align: center !important;display:block;margin:0 0 -20px 0;"><?php the_field('footer_tagline_heading');?></span> <br><br> <small class="<?php echo strlen(get_field('footer_tagline_content')) < 1 ? 'hidden' : ''; ?>" style="font-size: 14px;display:block;line-height:1.5;padding:10px;"><?php the_field('footer_tagline_content'); ?></small> </h4>
							</div>
						<?php endwhile; ?>
						<!-- end of the loop -->

						<?php wp_reset_postdata(); ?>
					<?php endif; ?>

				</div>
				<?php endif;?>
				<div class="col-md-3 col-md-offset-1">
					<h2>Main Menu</h2>
					<nav class="footer-navigation">
							<?php wp_nav_menu(array('theme_location' => 'menu-2', 'menu_id' => 'footer-menu')); ?>
					</nav>

					<br><br><br>
				</div>
			</div>
		</div>
	</footer>
	<footer id="social">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<small>&copy;<?php echo date('Y');?> WellnessWorks at Trinity Centre, LLC., All Rights Reserved. Contact us at <a
							href="tel:18003693556" style="color:#fff;">1.800.369.3556</a></small>
				</div>
				<div class="col-md-4 text-right">
					<p><a href="https://www.facebook.com/wellnessworkshq/" target="_blank" style="color:#fff;display: inline;padding-right:10px;">  <i class="fa fa-facebook-official" aria-hidden="true"></i></a>
						<a href="https://twitter.com/WellnessWorksHQ" target="_blank" style="color:#fff;display: inline;padding-right:10px;"> <i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a href="https://www.linkedin.com/company/wellnessworks-at-trinity-place" target="_blank" style="color:#fff;display: inline;padding-right:10px;"> <i class="fa fa-linkedin" aria-hidden="true"></i></a>
						<a href="https://www.instagram.com/wellnessworks.nyc/" target="_blank" style="color:#fff;display: inline;padding-right:10px;"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
					</p>
				</div>
			</div>
		</div>
	</footer>
</div><!-- #page -->

<script
	src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 837096065;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/837096065/?guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Start of HubSpot Embed Code -->
  <script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3922905.js"></script>
<!-- End of HubSpot Embed Code -->
<?php wp_footer(); ?>

</body>
</html>
