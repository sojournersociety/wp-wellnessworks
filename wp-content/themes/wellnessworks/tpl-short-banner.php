<?php /* Template Name: Short Banner Template */

get_header();
?>


<div id="banner"
     style="min-height:150px;background-image:url(
     <?php
     while ( have_posts() ) : the_post();
         if(has_post_thumbnail()) {
             echo the_post_thumbnail_url('full');
         } else {
             echo get_template_directory_uri().'/images/banner.png';
         }
     endwhile; // End of the loop.
     ?>
         )">
</div>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <?php
        while ( have_posts() ) : the_post();

            get_template_part( 'template-parts/content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;

        endwhile; // End of the loop.
        ?>

    </main><!-- #main -->
</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
