<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="container">
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
    </div>

    <div class="entry-content">
        <?php
        //the_content();

        wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wellnessworks' ),
            'after'  => '</div>',
        ) );
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <?php the_field('the_wellnessworks_concept'); ?>
                </div>
                <div class="col-md-5">
                    <br>
                    <?php if(strlen(get_field('wellnessworks_right_image')) > 0):?>
                        <img src="<?php the_field('wellnessworks_right_image')?>" style="width:100%;max-width:max-content;display:block;" alt="">
                    <?php endif;?>
                </div>
                <div class="col-md-12">
                    <hr class="divider">
                </div>
            </div>
            <div class="row">

                <div class="col-md-7 pull-right">

                    <?php the_field('the_neighborhood'); ?>
                </div>
                <div class="col-md-5 pull-left">
                    <br>
                    <?php if(strlen(get_field('neighborhood_left_image')) > 0):?>
                        <img src="<?php the_field('neighborhood_left_image')?>" style="width:100%;max-width:max-content;display:block;" alt="">
                    <?php endif;?>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <hr class="divider">
                </div>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <?php the_field('the_founders'); ?>
                </div>
                <div class="col-md-5">
                    <br>
                    <?php if(strlen(get_field('founders_image')) > 0):?>
                        <img src="<?php the_field('founders_image')?>" style="width:100%;max-width:max-content;display:block;" alt="">
                    <?php endif;?>
                </div>
                <div class="col-md-12">
                    <br><br>
                    <div class="well text-center">
                        <h1><?php the_field('schedule_a_tour_headline'); ?></h1>
                        <br>
                        <a href="/schedule-a-tour" class="btn btn-primary btn-lg">Schedule A Tour</a>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </div><!-- .entry-content -->

    <?php if ( get_edit_post_link() ) : ?>
        <footer class="entry-footer container">
            <?php
            edit_post_link(
                sprintf(
                /* translators: %s: Name of current post */
                    esc_html__( 'Edit %s', 'wellnessworks' ),
                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                ),
                '<span class="edit-link">',
                '</span>',
                null,
                'btn btn-primary btn'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-## -->
