<div class="container">
    <div class="col-md-12 text-center">
        <h2>Services</h2>
        <hr>
    </div>
    <div class="col-md-12">
        <br><br>
        <div class="row">
            <?php

            $images = get_field('services', 'options');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)

            if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                    <div class="col-xs-6 service-icons">
                        <img src="<?php echo wp_get_attachment_image_src( $image['ID'], 'full' )[0    ]; ?>" alt="">
                        <small><nobr><?php echo $image['caption']; ?></nobr></small>
                        <br><br>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <div class="clearfix"></div>
            <div class="col-md-12 text-center">
                <br>
                <div class="well">
                    <small>And More!</small>
                </div>
            </div>
        </div>
    </div>
</div>