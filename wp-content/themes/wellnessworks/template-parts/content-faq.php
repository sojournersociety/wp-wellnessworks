<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="container">
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
        <?php the_content(); ?>
    </div>

    <div class="entry-content container">
        <div class="row">
            <?php

            $args = [
                'post_type' => 'faq',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'order' => 'ASC',
                'orderby' => [
                   'meta_key' => 'sort_order'
                ]
            ];
            $the_query = new WP_Query($args); ?>

            <?php if ($the_query->have_posts()) : ?>
                <?php $counter = 0;?>

                <!-- the loop -->
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                <div class="col-md-6 faq">
                    <div class="well">
                        <h4><?php the_title(); ?></h4>
                        <div class="answer">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
                    <?php
                        $counter++;
                        echo $counter % 2 == 0 ? '<div class="clearfix"></div>' : '';
                    ?>
                <?php endwhile; ?>
                <!-- end of the loop -->

                <?php wp_reset_postdata(); ?>
            <?php endif; ?>
        </div>
    </div><!-- .entry-content -->

</article><!-- #post-## -->
