<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="container">
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
    </div>

    <div class="entry-content">
        <div class="container">
            <div class="row">
                <?php

                $args = [
                    'post_type' => 'post',
                    'post_status' => 'publish',
                    //'meta_key' => 'member_type',
                    //'meta_value' => 'leadership',
                    //'posts_per_page' => -1,
                    //'order' => 'ASC',
                    //'orderby' => [
                    //   'meta_key' => 'sort_order'
                    //]
                ];
                $the_query = new WP_Query($args); ?>

                <?php if ($the_query->have_posts()) : ?>


                    <!-- the loop -->
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="col-md-12">
                            <div class="row">
                                <?php if(strlen(get_the_post_thumbnail_url()) > 0):?>
                                <div class="col-md-4">
                                    <img src="<?php the_post_thumbnail_url('full')?>" style="width:100%;max-width:max-content;display:block;" alt="">
                                </div>
                                <?php endif;?>
                                <div class="<?php echo strlen(get_the_post_thumbnail_url()) > 0 ? 'col-md-8' : 'col-md-12'; ?>">
                                    <h2><a href="<?php the_permalink(); ?>"><?php the_title()?></a></h2>
                                    <p><?php the_excerpt();?></p>
                                    <a href="<?php the_permalink(); ?>" class="btn btn-default">Read More</a>
                                </div>
                            </div>
                            <hr>
                        </div>
                    <?php endwhile; ?>
                    <!-- end of the loop -->

                    <?php wp_reset_postdata(); ?>
                <?php endif; ?>
            </div>
        </div>
    </div><!-- .entry-content -->

    <?php if ( get_edit_post_link() ) : ?>
        <footer class="entry-footer container">
            <?php
            edit_post_link(
                sprintf(
                /* translators: %s: Name of current post */
                    esc_html__( 'Edit %s', 'wellnessworks' ),
                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
                ),
                '<span class="edit-link">',
                '</span>',
                null,
                'btn btn-primary btn'
            );
            ?>
        </footer><!-- .entry-footer -->
    <?php endif; ?>
</article><!-- #post-## -->
