<?php

$args = [
    'post_type' => 'page',
//    'post_status' => 'publish',
    'p' => 7,
    'posts_per_page' => 1,
];
$the_query = new WP_Query($args); ?>

<?php if ($the_query->have_posts()) : ?>


    <!-- the loop -->
    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <div class="container">
            <br><br>
            <div class="col-md-3 core-benefit">
                <img src="<?php the_field('space_to_heal_perk_image'); ?>" width="255" alt="">
                <div class="clearfix"></div>
                <?php the_field('space_to_heal_perk_content')?>
                <br><br>
            </div>
            <div class="col-md-3 core-benefit">
                <img src="<?php the_field('attentive_concierge_perk_image'); ?>" width="255" alt="">
                <div class="clearfix"></div>
                <?php the_field('attentive_concierge_perk_content'); ?>
                <br><br>
            </div>
            <div class="col-md-3 core-benefit">
                <img src="<?php the_field('home_for_practice_perk_image'); ?>" width="255" alt="">
                <div class="clearfix"></div>
                <?php the_field('home_for_practice_perk_content'); ?>
                <br><br>
            </div>
            <div class="col-md-3 core-benefit">
                <img src="<?php the_field('committed_community_perk_image') ?>" width="255" alt="">
                <div class="clearfix"></div>
                <?php the_field('committed_community_perk_content') ?>
                <br><br>
            </div>
        </div>
    <?php endwhile; ?>
    <!-- end of the loop -->

    <?php wp_reset_postdata(); ?>
<?php endif; ?>
