<?php
/**
 * Wellness Works functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Wellness_Works
 */

if (!function_exists('wellnessworks_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function wellnessworks_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on Wellness Works, use a find and replace
         * to change 'wellnessworks' to the name of your theme in all the template files.
         */
        load_theme_textdomain('wellnessworks', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'wellnessworks'),
            'menu-2' => esc_html__('Footer', 'wellnessworks'),
            'promo-1' => esc_html__('Promo Header', 'wellnessworks'),
            'promo-2' => esc_html__('Promo Footer', 'wellnessworks'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('wellnessworks_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif;
add_action('after_setup_theme', 'wellnessworks_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wellnessworks_content_width()
{
    $GLOBALS['content_width'] = apply_filters('wellnessworks_content_width', 640);
}

add_action('after_setup_theme', 'wellnessworks_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wellnessworks_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'wellnessworks'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'wellnessworks'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'wellnessworks_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function wellnessworks_scripts()
{
    wp_enqueue_style('wellnessworks-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('wellnessworks-style', get_stylesheet_uri());
    wp_enqueue_script('wellnessworks-bootstrap-scripts', get_template_directory_uri() . '/javascripts/bootstrap.js', array(), null, true);
    wp_enqueue_script('wellnessworks-scripts', get_template_directory_uri() . '/javascripts/site.js', array(), null, true);
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'wellnessworks_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


add_action( 'init', 'create_suite' );
function create_suite() {
    register_post_type( 'suite',
        array(
            'labels' => array(
                'name' => __( 'Suites' ),
                'singular_name' => __( 'Suite' )
            ),
            'public' => true,
            'has_archive' => true,
        )
    );
}

add_action( 'init', 'create_faq' );
function create_faq() {
    register_post_type( 'faq',
        array(
            'labels' => array(
                'name' => __( 'FAQs' ),
                'singular_name' => __( 'FAQ' )
            ),
            'public' => true,
            'has_archive' => false,
        )
    );
}

function with_login() {
    // default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'

    // open the <ul>, set 'menu_class' and 'menu_id' values
    $wrap  = '<ul id="%1$s" class="%2$s">';

    // get nav items as configured in /wp-admin/
    $wrap .= '%3$s';

    // the static link
    $wrap .= '<li class="my-static-link"><a href="'.PORTAL.'">Login</a></li>';
    $wrap .= '<li class="my-static-link"><a href="/schedule-a-tour" class="btn btn-default btn-yellow" style="padding:8px 5px 5px;border:none;line-height: 1;margin-top:-3px;">Schedule a tour</a></li>';

    // close the <ul>
    $wrap .= '</ul>';

    // return the result
    return $wrap;
}

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
    $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
    return $html;
}

add_action( 'init', 'codex_product_init' );
/**
 * Register a product post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_product_init() {
    $labels = array(
        'name'               => _x( 'Products', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Product', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Products', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Product', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'product', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Product', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Product', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Product', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Product', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Products', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Products', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Products:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No products found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No products found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'your-plugin-textdomain' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'product' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'page-attributes' )
    );

    register_post_type( 'product', $args );
}

if(function_exists('acf_add_options_page')) {
    acf_add_options_page();
}
