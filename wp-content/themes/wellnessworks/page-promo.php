<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */

get_header(); ?>
    <div id="banner" class="home"
         style="background-image:url(
         <?php
         while ( have_posts() ) : the_post();
             if(has_post_thumbnail()) {
                 echo the_post_thumbnail_url('full');
             } else {
                 echo get_template_directory_uri().'/images/banner.png';
             }
         endwhile; // End of the loop.
         ?>
             );background-position: top center !important;">
    </div>
    <div class="container">
        <div class="col-md-8 col-md-offset-2 text-center">
            <h1>Full Service Suites for the <br> Practitioner Community <br> <a href="/contact" class="btn btn-primary btn-lg">Learn More</a></h1>
            <br><br>
        </div>



<!--        <hr class="divider col-md-8 col-md-offset-2">-->


    </div>
        <div style="background:#f2f2f2;">
            <div class="container">
                <div class="col-md-12 text-center">
                    <br><br>
                    <img src="<?php echo get_template_directory_uri().'/images/ico-space.png'?>" width="255" alt="">
                </div>
                <div class="col-md-6 text-center">
                    <h2 style="text-transform: capitalize">Well appointed <br> suites</h2>
                    <p class="lead">seamless online reservation for part time <br> hourly daily and monthly use</p>
                </div>
                <div class="col-md-6 text-center">
                    <h2 style="text-transform: capitalize">Full service suites and <br> <nobr>enterprise solutions</nobr></h2>
                    <p class="lead">We are happy to work with you in designing a service package best suited for your practice.</p>
                </div>
                <div class="col-md-12">
                    <hr class="divider">
                    <br>
                    <h1 class="text-center">
                        Want To Learn More? <br>
                        <a href="/schedule-a-tour" class="btn btn-primary btn-lg">Book a Tour</a>
                    </h1>
                    <br><br>
                </div>
            </div>
        </div>
    <div class="pull-quote">
        <div class="container text-center">
            <div class="col-md-12">
                <br>
                <img src="<?php echo get_template_directory_uri().'/images/ico-concierge.png'?>" width="100" alt="">
                <br>
                <h2>Concierge Services Include:</h2>
                <p class="lead">
                    Full time reception <br>
                    Appt management <br>
                    Client check in <br>
                    Bookkeeping <br>
                    Storage and more
                </p>
                <a href="/contact-us" class="btn btn-default btn-lg">Contact Us Now</a>
                <br><br>
            </div>
        </div>
    </div>
<!--    <hr class="divider col-md-8 col-md-offset-2">-->
<!--    <div class="col-md-12">-->
<!--        <h1 class="text-center">More than a co-working space</h1>-->
<!--        <br><br>-->
<!--    </div>-->
<?php //get_template_part('template-parts/content', 'four-core'); ?>




    <div class="clearfix"></div>
<?php
//get_sidebar();
get_template_part('footer', 'promo');