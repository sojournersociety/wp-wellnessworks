<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Wellness_Works
 */

get_header();
//get_template_part('header', 'promo');
?>

    <div id="banner" class="page">
    </div>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">

            <?php
            while ( have_posts() ) : the_post();

                get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>

        </main><!-- #main -->
    </div><!-- #primary -->

    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3>Visit Our Locations</h3>

                <ul class="location-list api-locations">
                    <img src="<?php echo get_template_directory_uri().'/images/loader.gif'; ?>" style="width:100px;display:block;" alt="">
                </ul>

            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="well">
                        <p>
                            <strong>Call us at</strong><br>
                            <a style="color:inherit;" href="tel:18003693556">1.800.369.3556</a>
                        </p>
                    </div>
                </div>
                <h3>Contact Us</h3>
                <div class="row">
                    <br>
                    <div class="col-md-12">
                        <!--[if lte IE 8]>
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                        <![endif]-->
                        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                        <script>
                            hbspt.forms.create({
                                css: '',
                                portalId: '3922905',
                                formId: 'ded28c62-e843-4cd8-aa58-2e8c8e440a33'
                            });
                        </script>
                    </div>
<!--                    <div class="embed-contact-form">-->
<!--                        <div style="background:#f2f2f2;padding:40px;"><img src="--><?php //echo get_template_directory_uri().'/images/loader.gif'; ?><!--" style="width:200px;display:block;margin:25px auto;" alt=""></div>-->
<!--                    </div>-->
                </div>
            </div>
        </div>
    </div>

<?php
get_sidebar();
get_footer();
//get_template_part('footer', 'promo');
