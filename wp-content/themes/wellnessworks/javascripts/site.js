$(document).ready(function(){
    //Make Suite Description height the same
    var descHeight = 0;
    $('.ideal-for').each(function(){
       var currentHeight = $(this).height();
        if(currentHeight > descHeight) {
            descHeight = currentHeight;
        }
    });
    $('.ideal-for').css('height', descHeight);

    //Make FAQs height the same
    var wellHeight = 0;
    $('.faq').each(function(){
        var currentHeight = $(this).height();
        if(currentHeight > wellHeight) {
            wellHeight = currentHeight;
        }
    });
    $('.faq .well').css('min-height', wellHeight);

});

var Product = {
    general: function(){
        var portalURL = $('meta[name="portal"]').attr('content');
        $.ajax({
            type: 'GET',
            url:portalURL+'/api/products',
            success:function(data){
                $('.api-products').html(data);
                Product.align();
            },
            error:function(){
                $('.api-products').append('<div class="alert alert-danger"><strong>Sorry!</strong> There seems to be an error!</div>');
            }
        });
    },
    align: function(){
        setInterval(function(){
            var divHeight = 0;
            $('.product-list .three-tier').each(function(){
                var currentHeight = $(this).height();
                if(currentHeight > divHeight) {
                    divHeight = currentHeight;
                }
            });
            $('.product-list .three-tier').css('min-height', divHeight);
        }, 500);
    },
    services: function(){
        var portalURL = $('meta[name="portal"]').attr('content');
        $.ajax({
            type: 'GET',
            url:portalURL+'/api/product-services',
            success:function(data){
                $('.api-product-services').html(data);
            },
            error:function(){
                $('.api-product-services').append('<div class="alert alert-danger"><strong>Sorry!</strong> There seems to be an error!</div>');
            }
        });
    }
}

var Location = {
    general: function(){
        var portalURL = $('meta[name="portal"]').attr('content');
        $.ajax({
            type: 'GET',
            url:portalURL+'/api/locations',
            success:function(data){
                $('.api-locations').html(data);
            },
            error:function(){
                $('.api-locations').append('<div class="alert alert-danger"><strong>Sorry!</strong> There seems to be an error!</div>');
            }
        });
    }
}

var Suite = {
    general: function(){
        var portalURL = $('meta[name="portal"]').attr('content');
        $.ajax({
           type:'GET',
            url:portalURL+'/api/suites',
            success:function(data){
                $('.api-suites').html(data);
            },
            error:function(){
                $('.api-suites').append('<div class="alert alert-danger"><strong>Sorry!</strong> There seems to be an error!</div>');
            }
        });
    }
}

var Form = {
    contact: function(){
        var portalURL = $('meta[name="portal"]').attr('content');
        var origHTML = $('.embed-contact-form').html();
        $.ajax({
            type: 'GET',
            url:portalURL+'/api/form/contact',
            success:function(data){
                setTimeout(function(){
                    $('.embed-contact-form').html(data);
                    $('.embed-contact-form').append('<div class="loader hidden">'+origHTML+'</div>');
                }, 2000);
            },
            error:function(){
                $('.embed-contact-form').append('<div class="alert alert-danger"><strong>Sorry!</strong> There seems to be an error!</div>');
            }
        });
    },
    postContact: function(){
        $('.embed-contact-form').on('submit', 'form', function(e){
            e.preventDefault();
            var form = $(this);
            var portalURL = $('meta[name="portal"]').attr('content');
            form.find('input[name="_token"]').val($('meta[name="token"]').attr('content'));
            var url = form.attr('action');
            var data = form.serialize();
            $.ajax({
                method:'POST',
                url:portalURL+'/api/form/contact/post',
                data: form.serialize(),
                beforeSend:function(){
                    $('.embed-contact-form .loader').removeClass('hidden');
                    $('.embed-contact-form form').addClass('hidden');
                },
                success:function() {
                    setTimeout(function(){
                        //Form.contact();
                    }, 3000);
                },
                complete: function(){
                    setTimeout(function(){
                        $('.embed-contact-form').parent().find('.alert').removeClass('hidden');
                    }, 3000);
                    //setTimeout(function(){
                    //    $('.embed-contact-form').parent().find('.alert').addClass('hidden');
                    //}, 6000);
                }
            });
        });
    },
    tour: function(){
        var portalURL = $('meta[name="portal"]').attr('content');
        var origHTML = $('.embed-tour-form').html();
        $.ajax({
            type: 'GET',
            url:portalURL+'/api/form/tour/',
            success:function(data){
                setTimeout(function(){
                    $('.embed-tour-form').html(data);
                    $('.embed-tour-form').append('<div class="loader hidden">'+origHTML+'</div>');
                }, 2000);
            },
            error:function(){
                $('.embed-tour-form').append('<div class="alert alert-danger"><strong>Sorry!</strong> There seems to be an error!</div>');
            }
        });
    },
    postTour: function(){
        $('.embed-tour-form').on('submit', 'form', function(e){
            e.preventDefault();
            var form = $(this);
            form.find('input[name="_token"]').val($('meta[name="token"]').attr('content'));
            var url = form.attr('action');
            var data = form.serialize();
            var portalURL = $('meta[name="portal"]').attr('content');
            $.ajax({
                method:'POST',
                url:portalURL+'/api/form/tour/post',
                data: form.serialize(),
                beforeSend:function(){
                    $('.embed-tour-form .loader').removeClass('hidden');
                    $('.embed-tour-form form').addClass('hidden');
                },
                success:function() {
                    setTimeout(function(){
                        //Form.tour();
                    }, 3000);
                },
                complete: function(){
                    setTimeout(function(){
                        $('.embed-tour-form').parent().find('.alert').removeClass('hidden');
                    }, 3000);
                    //setTimeout(function(){
                    //    $('.embed-tour-form').parent().find('.alert').addClass('hidden');
                    //}, 6000);
                }
            });
        });
    }
}

Product.general();
Product.services();

Location.general();

Suite.general();

Form.contact();
Form.postContact();
Form.tour();
Form.postTour();